import { Component, OnInit } from '@angular/core';
import { ListItem } from './models/list-item.model';
import { ListService } from './services/list/list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  listNameSearchText: string = '';
  lists: ListItem[] = [];
  listItemWhoseDetailsClicked: ListItem | null = null;

  constructor(private listService: ListService) {}

  ngOnInit(): void {
    this.lists = this.listService.fetchLists();
  }

  handleListItemDetailsClickByUser(listItem: ListItem) {
    if (listItem.id === this.listItemWhoseDetailsClicked?.id) {
      this.listItemWhoseDetailsClicked = null;
    } else {
      this.listItemWhoseDetailsClicked = listItem;
    }
  }

  handleListNameSearchTextInputEvent() {
    this.listItemWhoseDetailsClicked = null;
  }
}
