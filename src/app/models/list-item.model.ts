export interface ListItem {
  id: string;
  date: string;
  name: string;
  entitiesCount: number;
  actions: ('email' | 'share' | 'edit' | 'delete')[];
  details: string[];
}
