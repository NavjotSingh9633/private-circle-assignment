import { Pipe, PipeTransform } from '@angular/core';
import { ListItem } from 'src/app/models/list-item.model';

@Pipe({
  name: 'listFilter',
})
export class ListFilterPipe implements PipeTransform {
  transform(lists: ListItem[], ...args: string[]): ListItem[] {
    let listSearchTextName: string = args[0];

    if (listSearchTextName == '') {
      return lists;
    }

    let filteredLists: ListItem[] = lists.filter((listItem) => {
      return listItem.name
        .toLowerCase()
        .indexOf(listSearchTextName.toLowerCase()) != -1
        ? true
        : false;
    });

    return filteredLists;
  }
}
